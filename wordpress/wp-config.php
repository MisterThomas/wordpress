<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x#F?_yhtBNNnFi]{fMf?d1 !q%7e[6mwg*e4R%wC=*o!Cj_+DUv[omRL3%%F12[h');
define('SECURE_AUTH_KEY',  '(eT}P{4ocJQY]<Xyo4Sj9wrmw^jVb@ghU/V1fxm[y}<A(zkm^AGt;rpdD`J(]4?6');
define('LOGGED_IN_KEY',    '[VfHoI/LlP~}Me~-ALJG?auJ,z:-dykDh$8T8Xl}V/uj|7LIGr.VV5J5HJ(_i^[$');
define('NONCE_KEY',        'VG`xf5zO3~+2oH*nZr9pKZ>s4$Luc*qT=1IxZ_l:#F67na:01cjUs>rt=EXNOl5A');
define('AUTH_SALT',        '`)2nOD9tRa IdaeM*ni`4d{ys{}k*J:T6~B D:kK2#.DH5C78p.B;N+o94y[P[-W');
define('SECURE_AUTH_SALT', '6O4MpK{_xVHE*v(Q8.F TOD(e=Edzf<#g3k5A>}IAK.j{_MQ h]?in37lOd]L<([');
define('LOGGED_IN_SALT',   'FdtUH|UlGb^-;&ob(T{1$<(?)ZztuGZbApkCC> So%.VzB9,&%=h^FrfW3B+5?eA');
define('NONCE_SALT',       'P#)|<*4w1f!/3`P9t.>ou8<585gR{JUiYruE6d_vxk@H92)>~(*Ywvr)lAL4]uBJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
